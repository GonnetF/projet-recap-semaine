package co.simplon.promo16;

import java.util.ArrayList;

import co.simplon.promo16.classproject.baseVariable;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        /*
        1. Dans le App.java, créer quelques variables et les afficher avec un sysout : une variable qui
         contiendra votre age, une variable qui contiendra le nom de la promo, une variable qui contiendra 
         si c'est vrai ou faux que vous aimez java, une variable qui contiendra un tableau avec les langages que vous
          connaissez dedans
        */

        baseVariable.first(23, "Promo16", true);

        baseVariable.withParameter("non", 18);

        System.out.println(baseVariable.withReturn()); 




    }
}
